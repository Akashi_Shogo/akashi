package jp.alhinc.akashi_shogo.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class CalculateSales {

	public static void main(String[] args) {

		HashMap<String, String> branchMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		if (args.length == 0 || args.length >= 2) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if (!readFileBranch(args[0], branchMap, salesMap)) {// 支店定義ファイルの読み込みメソッド読み込み
			return;
		}
		if (!readFileSales(args[0], branchMap, salesMap)) {// 集計読み込みメソッド
			return;
		}
		if (!outPutFile(args[0], "branch.out", branchMap, salesMap)) { // 出力メソッドの呼び出し
			return;
		}
	}

	public static boolean readFileBranch(String dirpath, HashMap<String, String> branchMap,
			HashMap<String, Long> salesMap) {

		BufferedReader br = null;

		try {
			File file = new File(dirpath, "branch.lst");

			// 支店定義ファイル(branch.lst)の有無確認
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String branch;

			while ((branch = br.readLine()) != null) {
				String[] test = branch.split(",");

				// 支店定義ファイル1行の要素数が2個に限定

				if (!(test.length == 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchMap.put(test[0], test[1]);
				if (!(test[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				salesMap.put(test[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean readFileSales(String dirpath, HashMap<String, String> branchMap,
			HashMap<String, Long> salesMap) {

		File dir = new File(dirpath);
		File dirfiles[] = dir.listFiles();
		ArrayList<File> sales = new ArrayList<File>();

		for (int i = 0; i < dirfiles.length; i++) {
			if (dirfiles[i].getName().matches("\\d{8}.rcd$") && (dirfiles[i].isFile())) {
				sales.add(dirfiles[i]);
			}
		}

		// 連番チェック用の拡張子の削除

		ArrayList<String> fileNames = new ArrayList<String>();
		for (int i = 0; i < sales.size(); i++) {
			File f = sales.get(i);
			String file = f.getName();
			String fName = file.substring(0, file.lastIndexOf("."));
			fileNames.add(fName);
		}

		// 連番チェック

		for (int i = 1; i < fileNames.size(); i++) {
			int x = Integer.parseInt(fileNames.get(i));
			int y = Integer.parseInt(fileNames.get(i - 1));
			int z = x - y;

			if (z != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}

		BufferedReader br1 = null;
		for (int i = 0; i < sales.size(); i++) {
			try {
				FileReader fr1 = new FileReader(sales.get(i));
				br1 = new BufferedReader(fr1);

				ArrayList<String> codeSales = new ArrayList<String>();

				String dirfiles1;

				while ((dirfiles1 = br1.readLine()) != null) {// dirfile1の行の読み込み
					codeSales.add(dirfiles1);
				}

				// 売上ファイルが2行以外の時
				if (codeSales.size() != 2) {
					System.out.println(sales.get(i).getName() + "のフォーマットが不正です");
					return false;
				}
				// 売上げ金額が数字以外のとき
				if (!(codeSales.get(1).matches("^[0-9]*$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				// 支店コードの該当がないとき
				if (salesMap.get(codeSales.get(0)) == null) {
					System.out.println(sales.get(i).getName() + "の支店コードが不正です");
					return false;
				}
				// Long型に変換
				long total = Long.parseLong(codeSales.get(1));// Long型に変換
				long totalSales = salesMap.get(codeSales.get(0)) + total;

				// 合計が10桁超えたときの処理
				if (totalSales >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				salesMap.put(codeSales.get(0), totalSales);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if (br1 != null) {
					try {
						br1.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}

	// ファイル出力メソッド
	public static boolean outPutFile(String dirpath, String fileName, HashMap<String, String> branchMap,
			HashMap<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File file = new File(dirpath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (Map.Entry<String, String> entry : branchMap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
